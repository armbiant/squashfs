package squashfs

import (
	"io"
	"io/fs"
	"time"
)

// namedInode is an inode + a file name.  Shocking.
// It is the base for the exported file-ish things.
type namedInode struct {
	ino  *inode
	name string
}

func (ni namedInode) ReadLink() (string, error) {
	return ni.ino.readLink()
}

func (ni namedInode) Stat() (fs.FileInfo, error) {
	return ni, nil
}

func (ni namedInode) IsDir() bool {
	return ni.ino.isDir()
}

func (ni namedInode) Mode() fs.FileMode {
	return ni.ino.mode()
}

// Name returns the file's base name
func (ni namedInode) Name() string {
	return ni.name
}

// Size returns the file's size
func (ni namedInode) Size() int64 {
	return int64(ni.ino.sz)
}

// ModTime returns the file's latest modified time. Note that squashfs stores
// this as a int32, which means it'll stop working after 2038.
func (ni namedInode) ModTime() time.Time {
	return time.Unix(int64(ni.ino.mtime), 0)
}

type SysStat struct {
	Mode    fs.FileMode
	Nlink   uint32
	Ino     uint32
	Mtime   time.Time
	Size    int64
	Blocks  int64
	Blksize int64
}

// Sys returns an object containing lower level stat information.
// Most of it is a quasi useful lie.
func (ni namedInode) Sys() any {
	return SysStat{
		Mode:    ni.ino.mode(),
		Nlink:   ni.ino.nLink,
		Ino:     ni.ino.ino,
		Mtime:   ni.ModTime(),
		Size:    int64(ni.ino.sz),
		Blocks:  int64(len(ni.ino.blocks)),
		Blksize: int64(ni.ino.sb.blockSize),
	}
}

func (ni namedInode) Close() error {
	return nil
}

// Unhandled is an fs.File for file types supported by squashfs that this library does not handle.
// Think devices, FIFOs, and the like.
type Unhandled struct {
	namedInode
}

func (s Unhandled) Read([]byte) (int, error) {
	return 0, fs.ErrInvalid
}

// File is an  fs.File + io.ReaderAt and friends.
type File struct {
	*io.SectionReader
	namedInode
}

// Dir is an fs.File that supports fs.ReadDirFile
type Dir struct {
	r *dirReader
	namedInode
}

func (d *Dir) Read([]byte) (int, error) {
	return 0, fs.ErrInvalid
}

func (d *Dir) Close() error {
	d.r = nil
	return nil
}

func (d *Dir) ReadDir(n int) ([]fs.DirEntry, error) {
	if d.r == nil {
		dr, err := d.ino.sb.dirReader(d.ino)
		if err != nil {
			return nil, err
		}
		d.r = dr
	}

	return d.r.ReadDir(n)
}

// Ensure File respects fs.File & others
var _ fs.File = (*File)(nil)
var _ io.ReaderAt = (*File)(nil)
var _ fs.ReadDirFile = (*Dir)(nil)
var _ fs.FileInfo = (*namedInode)(nil)

// open returns a fs.File for a given inode. If the file is a directory, the returned object will implement
// fs.ReadDirFile. If it is a regular file it will also implement io.Seeker.
// When we want to start handling more file types they will be added here.
func (ino *inode) open(name string) fs.File {
	switch ino.typ {
	case DirType, XDirType:
		return &Dir{nil, namedInode{name: name, ino: ino}}
	case FileType, XFileType:
		return &File{io.NewSectionReader(ino, 0, int64(ino.sz)), namedInode{name: name, ino: ino}}
	default:
		return Unhandled{namedInode{name: name, ino: ino}}
	}
}
