package squashfs

import (
	"bytes"
	"fmt"
	"github.com/klauspost/compress/zlib"
	"github.com/klauspost/compress/zstd"
	"io"
	"sync"
)

// Compression tracks what type of compression a squashfs is using.
// Out of the box, we support GZip and ZSTD using github.com/klauspost/compress.
// Additional types may be suported by default later, and you can always
// register your own.
type Compression uint16

const (
	GZip Compression = iota + 1
	LZMA
	LZO
	XZ
	LZ4
	ZSTD
)

// Decompressor takes an input buffer and a destination buffer and returns the destination buffer filled with data and an error
// It matches the function signature of github.com/klauspost/compress/zstd Reader.DecodeAll.
// The library internals will eventually leverage it to optimize buffer reuse and caching.
type Decompressor func([]byte, []byte) ([]byte, error)

var decompressHandlers = map[Compression]Decompressor{}

var decompressMux = &sync.Mutex{}

func init() {
	zs, _ := zstd.NewReader(nil,
		zstd.WithDecoderLowmem(true),
		zstd.WithDecoderMaxWindow(128<<20),
		zstd.WithDecoderConcurrency(0))
	RegisterDecompressor(ZSTD, func(buf, dst []byte) ([]byte, error) { return zs.DecodeAll(buf, dst) })
	RegisterDecompressor(GZip, MakeDecompressorErr(zlib.NewReader))
}

func (s Compression) String() string {
	switch s {
	case GZip:
		return "GZip"
	case LZMA:
		return "LZMA"
	case LZO:
		return "LZO"
	case XZ:
		return "XZ"
	case LZ4:
		return "LZ4"
	case ZSTD:
		return "ZSTD"
	}
	return fmt.Sprintf("Compression(%d)", s)
}

// RegisterDecompressor can be used to register a Decompressor for squashfs.
// GZip and ZSTD are supported by default.  You can register additional
// decompressor types before opening squashfs archives that use them.  Once
// registered, decompressors cannot be deregistered, only replaced.
func RegisterDecompressor(method Compression, dcomp Decompressor) {
	decompressMux.Lock()
	decompressHandlers[method] = dcomp
	decompressMux.Unlock()
}

// MakeDecompressor allows using a decompressor made for archive/zip with
// SquashFs. It has some overhead as instead of simply dealing with buffer this
// uses the reader/writer API, but should allow to easily handle some formats.
//
// Example use:
// * squashfs.RegisterDecompressor(squashfs.ZSTD, squashfs.MakeDecompressor(zstd.ZipDecompressor()))
// * squashfs.RegisterDecompressor(squashfs.LZ4, squashfs.MakeDecompressor(lz4.NewReader)))
func MakeDecompressor(dec func(r io.Reader) io.ReadCloser) Decompressor {
	return func(buf, dst []byte) ([]byte, error) {
		r := bytes.NewReader(buf)
		p := dec(r)
		defer p.Close()
		var w *bytes.Buffer
		if dst != nil {
			w = bytes.NewBuffer(dst[:0])
		} else {
			w = &bytes.Buffer{}
		}
		_, err := io.Copy(w, p)
		return w.Bytes(), err
	}
}

// MakeDecompressorErr is similar to MakeDecompressor but the factory method also
// returns an error.
//
// Example use:
// * squashfs.RegisterDecompressor(squashfs.LZMA, squashfs.MakeDecompressorErr(lzma.NewReader))
// * squashfs.RegisterDecompressor(squashfs.XZ, squashfs.MakeDecompressorErr(xz.NewReader))
func MakeDecompressorErr(dec func(r io.Reader) (io.ReadCloser, error)) Decompressor {
	return func(buf, dst []byte) ([]byte, error) {
		r := bytes.NewReader(buf)
		p, err := dec(r)
		if err != nil {
			return nil, err
		}
		defer p.Close()
		var w *bytes.Buffer
		if dst != nil {
			w = bytes.NewBuffer(dst[:0])
		} else {
			w = &bytes.Buffer{}
		}
		_, err = io.Copy(w, p)
		return w.Bytes(), err
	}
}
