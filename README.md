# squashfs

This is a read-only implementation of squashfs that conforms to the `io/fs` interfaces.

# Example use

```go
package main

import (
	"os"
	"net/http"
	"gitlab.com/rackn/squashfs"
)

func main() {
	fi, _ := os.Open("file.squashfs")
	sqfs, _ := squashfs.Open(fi)
	// sqfs is a regular fs.FS

	http.Handle("/", http.FileServer(http.FS(sqfs)))
	// etc...
}
```

You can find more looking at the test file.

# File format

Some documentation is available online on SquashFS.

* https://dr-emann.github.io/squashfs/squashfs.html

# TODO

* Access to directories do not currently use indexes and can be slow for random file accesses in very large directories.
* A block caching scheme is needed to handle high-traffic sections of the file.  That could be fun.
