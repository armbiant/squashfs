package squashfs

import (
	"encoding/binary"
	"errors"
	"io"
	"io/fs"
)

type inode struct {
	sb    *FS
	typ   inoType
	perm  uint16
	mtime int32
	ino   uint32 // inode number

	startBlock uint64
	nLink      uint32
	sz         uint64 // Careful, actual on disk size varies depending on type
	offset     uint32 // uint16 for directories
	parentIno  uint32 // for directories
	symTarget  []byte // The target path this symlink points to
	idxCount   uint16 // index count for advanced directories
	xattrIdx   uint32 // xattr table index (if relevant)

	// fragment
	fragBlock  uint32
	fragOffset uint32

	// file blocks (some have value 0x1001000)
	blocks       []uint32
	blocksOffset []uint64
}

func (ino *inode) readCommon(ord binary.ByteOrder, r *tableReader) error {
	var buf [16]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.typ = inoType(ord.Uint16(buf[0:2]))
	ino.perm = ord.Uint16(buf[2:4])
	// skip 2 byte uid and gid
	ino.mtime = int32(ord.Uint32(buf[8:12]))
	ino.ino = ord.Uint32(buf[12:16])
	return nil
}

func (ino *inode) readBasicDir(ord binary.ByteOrder, r *tableReader) error {
	var buf [16]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.startBlock = uint64(ord.Uint32(buf[0:4]))
	ino.nLink = ord.Uint32(buf[4:8])
	ino.sz = uint64(ord.Uint16(buf[8:10]))
	ino.offset = uint32(ord.Uint16(buf[10:12]))
	ino.parentIno = ord.Uint32(buf[12:16])
	return nil
}

func (ino *inode) readExtendedDir(ord binary.ByteOrder, r *tableReader) error {
	var buf [24]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.nLink = ord.Uint32(buf[0:4])
	ino.sz = uint64(ord.Uint32(buf[4:8]))
	ino.startBlock = uint64(ord.Uint32(buf[8:12]))
	ino.parentIno = ord.Uint32(buf[12:16])
	ino.idxCount = ord.Uint16(buf[16:18])
	ino.offset = uint32(ord.Uint16(buf[18:20]))
	ino.xattrIdx = ord.Uint32(buf[20:24])
	return nil
}

func (ino *inode) fillFileBlocks(ord binary.ByteOrder, r *tableReader) error {
	// try to find out how many block_sizes entries
	blocks := int(ino.sz / uint64(ino.sb.blockSize))
	if ino.fragBlock == 0xffffffff {
		// file does not end in a fragment
		if ino.sz%uint64(ino.sb.blockSize) != 0 {
			blocks += 1
		}
	}
	blockBuf := make([]byte, blocks*4)
	ino.blocks = make([]uint32, blocks)
	ino.blocksOffset = make([]uint64, blocks)
	runningOffset := uint64(0)
	if _, err := io.ReadFull(r, blockBuf); err != nil {
		return err
	}

	// read blocks
	for i := range ino.blocks {
		ino.blocks[i] = ord.Uint32(blockBuf[4*i : 4*(i+1)])
		ino.blocksOffset[i] = runningOffset
		runningOffset += uint64(ino.blocks[i]) & 0xfffff // 1MB-1, since max block size is 1MB
	}

	if ino.fragBlock != 0xffffffff {
		// this has a fragment instead of last block
		ino.blocks = append(ino.blocks, 0xffffffff) // special code
	}
	return nil
}

func (ino *inode) readBasicFile(ord binary.ByteOrder, r *tableReader) error {
	var buf [16]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.startBlock = uint64(ord.Uint32(buf[0:4]))
	ino.fragBlock = ord.Uint32(buf[4:8])
	ino.fragOffset = ord.Uint32(buf[8:12])
	ino.sz = uint64(ord.Uint32(buf[12:16]))
	return ino.fillFileBlocks(ord, r)
}

func (ino *inode) readExtendedFile(ord binary.ByteOrder, r *tableReader) error {
	var buf [40]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.startBlock = ord.Uint64(buf[0:8])
	ino.sz = ord.Uint64(buf[8:16])
	// buf[16:24] is space hidden by sparse file encoding.  Only the Linux kernel cares about it.
	ino.nLink = ord.Uint32(buf[24:28])
	ino.fragBlock = ord.Uint32(buf[28:32])
	ino.fragOffset = ord.Uint32(buf[32:36])
	ino.xattrIdx = ord.Uint32(buf[36:40])
	return ino.fillFileBlocks(ord, r)
}

func (ino *inode) readSymlink(ord binary.ByteOrder, r *tableReader) error {
	var buf [8]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return err
	}
	ino.nLink = ord.Uint32(buf[0:4])
	ino.sz = uint64(ord.Uint32(buf[4:8]))
	if ino.sz > 4096 {
		// why is symlink length even stored as u32 ?
		return errors.New("symlink target too long")
	}
	ino.symTarget = make([]byte, int(ino.sz))
	_, err := io.ReadFull(r, ino.symTarget)
	return err
}

func (sq *FS) getInodeRef(inor inodeRef) (*inode, error) {
	r, err := sq.newInodeReader(inor)
	if err != nil {
		return nil, err
	}
	ino := &inode{sb: sq}
	if err = ino.readCommon(sq.order, r); err != nil {
		return nil, err
	}

	switch ino.typ {
	case DirType:
		err = ino.readBasicDir(sq.order, r)
	case XDirType:
		err = ino.readExtendedDir(sq.order, r)
	case FileType:
		err = ino.readBasicFile(sq.order, r)
	case XFileType:
		err = ino.readExtendedFile(sq.order, r)
	case SymlinkType, XSymlinkType:
		err = ino.readSymlink(sq.order, r)
	}
	return ino, err
}

func (i *inode) ReadAt(p []byte, off int64) (int, error) {
	if !i.typ.IsRegular() {
		return 0, fs.ErrInvalid
	}
	if uint64(off) >= i.sz {
		// no read
		return 0, io.EOF
	}
	if uint64(off+int64(len(p))) > i.sz {
		p = p[:int64(i.sz)-off]
	}
	// we need to know what block to start with
	block := int(off / int64(i.sb.blockSize))
	offset := int(off % int64(i.sb.blockSize))
	n := 0
	for {
		var buf []byte

		// read block
		if i.blocks[block] == 0xffffffff {
			// this is a fragment, need to decode fragment
			// read table offset
			sub := int64(i.fragBlock) / 512 * 8
			blInfo := make([]byte, 8)
			_, err := i.sb.src.ReadAt(blInfo, int64(i.sb.fragTableStart)+sub)
			if err != nil {
				return n, err
			}

			// read table
			t, err := i.sb.newTableReader(int64(i.sb.order.Uint64(blInfo)), int(i.fragBlock%512)*16)
			if err != nil {
				return n, err
			}

			var start uint64
			var size uint32
			err = binary.Read(t, i.sb.order, &start)
			if err != nil {
				return n, err
			}
			err = binary.Read(t, i.sb.order, &size)
			if err != nil {
				return n, err
			}

			if size&0x1000000 == 0x1000000 {
				// no compression
				buf = make([]byte, size&(0x1000000-1))
				_, err = i.sb.src.ReadAt(buf, int64(start))
				if err != nil {
					return n, err
				}
			} else {
				// read fragment
				buf = make([]byte, size)
				_, err = i.sb.src.ReadAt(buf, int64(start))
				if err != nil {
					return n, err
				}

				// decompress
				buf, err = i.sb.decompress(buf, nil)
				if err != nil {
					return n, err
				}
			}

			if i.fragOffset != 0 {
				buf = buf[i.fragOffset:]
			}
		} else if i.blocks[block] == 0 {
			// this part of the file contains only zeroes
			buf = make([]byte, i.sb.blockSize)
		} else {
			buf = make([]byte, i.blocks[block]&0xfffff)
			_, err := i.sb.src.ReadAt(buf, int64(i.startBlock+i.blocksOffset[block]))
			if err != nil {
				return n, err
			}

			// check for compression
			if i.blocks[block]&0x1000000 == 0 {
				// compressed
				buf, err = i.sb.decompress(buf, nil)
				if err != nil {
					return n, err
				}
			}
		}

		// check offset
		if offset > 0 {
			buf = buf[offset:]
		}

		// copy
		l := copy(p, buf)
		n += l
		if l == len(p) {
			// end of copy
			return n, nil
		}

		// advance out ptr
		p = p[l:]

		// next block
		block += 1
		offset = 0
	}
}

// lookupRelativeInode finds the given inode in the directory
func (i *inode) lookupRelativeInode(name string) (*inode, error) {
	// TODO: handle indexes
	if !i.typ.IsDir() {
		return nil, fs.ErrInvalid
	}
	// basic dir, we need to iterate (cache data?)
	dr, err := i.sb.dirReader(i)
	if err != nil {
		return nil, err
	}
	for {
		ename, inoR, err := dr.next()
		if err != nil {
			if err == io.EOF {
				return nil, fs.ErrNotExist
			}
			return nil, err
		}

		if name == ename {
			// found, load the inode from its ref
			found, err := i.sb.getInodeRef(inoR)
			if err != nil {
				return nil, err
			}
			return found, nil
		}
	}
}

// mode returns the inode's mode as fs.FileMode
func (i *inode) mode() fs.FileMode {
	return unixToMode(uint32(i.perm)) | i.typ.Mode()
}

// isDir returns true if the inode is a directory inode.
func (i *inode) isDir() bool {
	return i.typ.IsDir()
}

// readLink returns the inode's link
func (i *inode) readLink() (string, error) {
	if !i.typ.IsSymlink() {
		return "", fs.ErrInvalid
	}
	return string(i.symTarget), nil
}
